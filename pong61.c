/* 
* FILE : pong61.c
* STUDENT : Alan Orcharton
* CLASS : CSCI E61 - Systems Programming
* DATE : December, 2012
* ASSIGNMENT : pset5 - Adversarial Network Pong
*
* DESCRIPTION : 
*
*   This file contains the updated code for pong61.
*
*   Phase 1: The objective of phase 1 was to handle loss of 
*            the connection. In my implementation, if a thread
*            detects loss due to a status code -1 the SAME thread
*            will retry the connection after a exponential backoff 
*            wait period. This is all done before the thread sends,
*            the &flag signal to the main thread (see phase 2). This
*            keeps the ball position sequence correct.
*
*    Phase 2: The objective of phase 2 was to handle delayed response.
*            I implemented this using threads. I used detached threads 
*            that destroy themselves once they have completed. In order
*            to control the speed at which the threads are created I
*            implemented a thread wait condition. The main thread will
*            start a thread, then wait for the condition &flag before 
*            continuing in the main program loop and creating a new thread.
*
*            The flag signal is only returned by a thread if the response
*            header indicates 200 OK. This way if the body is delayed a new
*            thread can start.
*
*
*    Phase 3: The objective of phase 3 was to reuse connections. I did not
*            limit the number of threads, but instead limited the number of
*            connection objects. The connection objects are allocated on the 
*            heap, so each one stays around after the thread that created it
*            has gone.
*
*            All references to connection objects are maintained within a 
*            conn_table array. Each new thread checks the connection table
*            for an available connection. If no connections are available
*            the thread will loop until an object is available.
*
*            The connection table is accessed by multiple threads, so a
*            mutex is used to control the access to the table.
*
*
*    Phase 4: The objective of phase 4 was to handle congestion control.
*            Some of the responses contain a STOP message and indicate
*            a time through which all new connections should be halted.
*
*            I implemented this using the pthread_cond_timedwait call.
*            Each thread calls this function before sending a request.
*
*            If any thread receives the STOP message in a response body
*            then a global hold variable (struct timespec) is updated
*            with the absolute time that the hold period is to end.
*
*            In most cases the times wait hold will have passed but in
*            halt situations all threads will block just before sending
*            a request until the specified halt period has passed.  
*
*            The hold global struct timespec is accessed by all threads,
*            so a mutex is also used here.
*
*
*    Phase 5: The objective of this phase was to find the problems
*            causing the client program to seg fault. 
*
*            Some of the responses returned by the server contain really
*            long headers. When the http_consume_headers function tries
*            to read such headers it attempts to access memory outside the
*            boundary of the response buffer.
*
*            To remedy this. I increased the size of the response buffer.
*            This file provides library functions to implement
* 
*/

#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/time.h>
#include <netdb.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <errno.h>
#include <signal.h>
#include <assert.h>
#include <pthread.h>
#include "serverinfo.h"


#define MAX_CONNECTIONS     20          //Number of allowed connection objects
#define RESPONSEBUFFSZ      25000       //Response buffer size
#define SECTONANO           1000000000  //Conversion constant seconds to nano-seconds
#define MICROSECTONANO      1000        //Micro seconds to nano seconds conversion
#define SECTOMICRO          1000000     //Seconds to microseconds conversion


static const char *pong_host = PONG_HOST;
static const char *pong_port = PONG_PORT;
static const char *pong_user = PONG_USER;

pthread_mutex_t lock = PTHREAD_MUTEX_INITIALIZER;  //lock mutex for condition flag
pthread_cond_t flag = PTHREAD_COND_INITIALIZER;    //a condition flag
    

pthread_mutex_t conn_tbl_lock = PTHREAD_MUTEX_INITIALIZER;  //the mutex for the condition table
pthread_mutex_t pause_lock = PTHREAD_MUTEX_INITIALIZER;     //the lock for the hold variable

pthread_cond_t hold_flag = PTHREAD_COND_INITIALIZER; //condition flag

//double pause_time = 0;


//sets attributes for threads
static void setattr(pthread_attr_t *attrp);      



// Timer and interrupt functions (defined and explained below)

double timestamp(void);
void sleep_for(double delay);
void interrupt_after(double delay);
void interrupt_cancel(void);




// HTTP connection management functions

// http_connection
//    This object represents an open HTTP connection to a server.
typedef struct http_connection {
    int fd;                 // Socket file descriptor

    int state;              // Response parsing status (see below)
    int status_code;        // Response status code (e.g., 200, 402)
    size_t content_length;  // Content-Length value
    int has_content_length; // 1 iff Content-Length was provided

    char buf[RESPONSEBUFFSZ];       // Response buffer
    size_t len;             // Length of response buffer

} http_connection;

//Data needed for threads
typedef struct ball_thread {
    struct addrinfo *ai;
    char url[BUFSIZ];
    size_t retry_count;
    int x;
    int y;
    int seq;

} ball_thread;


//connection table
http_connection *conn_table[MAX_CONNECTIONS];

//Global variable - the time to wait before sending requests
struct timespec hold_time;
    /* data */

// `http_connection::state` constants:
#define HTTP_REQUEST 0      // Request not sent yet
#define HTTP_INITIAL 1      // Before first line of response
#define HTTP_HEADERS 2      // After first line of response, in headers
#define HTTP_BODY    3      // In body
#define HTTP_DONE    (-1)   // Body complete, available for a new request
#define HTTP_CLOSED  (-2)   // Body complete, connection closed
#define HTTP_BROKEN  (-3)   // Parse error

// helper functions
char *http_truncate_response(http_connection *conn);
static int http_consume_headers(http_connection *conn, int eof);
static void retry_wait( size_t *pretry_count);
static void usage(void);
static void *move_ball(void *vpbt);
static http_connection *get_connection(const struct addrinfo *ai);
static void set_hold_time(double hold);

// http_connect(ai)
//    Open a new connection to the server described by `ai`.
//    create a new one if there is space for more in the
//    connection table.
http_connection *http_connect(const struct addrinfo *ai) {

    http_connection *conn = NULL;
    //check for available connections
    while((conn = get_connection(ai)) == NULL){
        sleep_for(0.1);
    }

    return conn;
}

//get an available connection from the connection table
//if none available but table has space, make a new one.
http_connection *get_connection(const struct addrinfo *ai){

    http_connection *next_conn = NULL;

    int i = 0;
    pthread_mutex_lock(&conn_tbl_lock);
    for(i = 0; i < MAX_CONNECTIONS; i++){
        if (!conn_table[i])
            break;
        //printf("conn table i %d\n",i);
        //if(conn_table[i])
          //  printf("State %d\n",conn_table[i]->state);
        if((conn_table[i]) && (conn_table[i]->state == HTTP_DONE))
            next_conn = conn_table[i]; 
    }
    pthread_mutex_unlock(&conn_tbl_lock);

    if((next_conn == NULL) && (i < MAX_CONNECTIONS)){
                    //make a new connection if less than max
           int fd = socket(AF_INET, SOCK_STREAM, 0);
            if (fd < 0) {
            perror("socket");
            exit(1);
            }   

            int yes = 1;
            (void) setsockopt(fd, SOL_SOCKET, SO_REUSEADDR, &yes, sizeof(int));

            int r = connect(fd, ai->ai_addr, ai->ai_addrlen);
            if (r < 0) {
                perror("connect");
                exit(1);
            }

            // construct an http_connection object for this connection
            next_conn = (http_connection *) malloc(sizeof(http_connection));
            next_conn->fd = fd;
            next_conn->state = HTTP_REQUEST;

            pthread_mutex_lock(&conn_tbl_lock);
            conn_table[i] = next_conn;
            pthread_mutex_unlock(&conn_tbl_lock); 

    }
    return next_conn;
    
}

// http_close(conn)
//    Close the HTTP connection `conn` and free its resources.
void http_close(http_connection *conn) {
    close(conn->fd);
    free(conn);
}


// http_send_request(conn, uri)
//    Send an HTTP POST request for `uri` to connection `conn`.
//    Exit on error.
void http_send_request(http_connection *conn, const char *uri) {
    assert(conn->state == HTTP_REQUEST || conn->state == HTTP_DONE);

    // prepare and write the request
    char reqbuf[BUFSIZ];
    size_t reqsz = sprintf(reqbuf,
                           "POST /%s/%s HTTP/1.0\r\n"
                           "Host: %s\r\n"
                           "Connection: keep-alive\r\n"
                           "\r\n",
                           pong_user, uri, pong_host);
    size_t pos = 0;
    while (pos < reqsz) {
        ssize_t nw = write(conn->fd, &reqbuf[pos], reqsz - pos);
        if (nw == 0)
            break;
        else if (nw == -1 && errno != EINTR && errno != EAGAIN) {
            perror("write");
            exit(1);
        } else if (nw != -1)
            pos += nw;
    }

    if (pos != reqsz) {
        fprintf(stderr, "connection closed prematurely\n");
        exit(1);
    }

    // clear response information
    conn->state = HTTP_INITIAL;
    conn->status_code = -1;
    conn->content_length = 0;
    conn->has_content_length = 0;
    conn->len = 0;
    //conn->retry_count = 0;
}


// http_receive_response(conn)
//    Receive a response from the server. On return, `conn->status_code`
//    holds the server's status code, and `conn->buf` holds the response
//    body, which is `conn->len` bytes long and has been null-terminated.
//    If the connection terminated prematurely, `conn->status_code`
//    is -1.
void http_receive_response(http_connection *conn) {
    assert(conn->state != HTTP_REQUEST);
    if (conn->state < 0)
        return;

    // parse connection (http_consume_headers tells us when to stop)
    size_t eof = 0;
    while (http_consume_headers(conn, eof)) {
        ssize_t nr = read(conn->fd, &conn->buf[conn->len], RESPONSEBUFFSZ);
        if (nr == 0)
            eof = 1;
        else if (nr == -1 && errno != EINTR && errno != EAGAIN) {
            perror("read");
            exit(1);
        } else if (nr != -1)
            conn->len += nr;
    }

    // null-terminate body
    conn->buf[conn->len] = 0;

    // Status codes >= 500 mean we are overloading the server and should exit.
    if (conn->status_code >= 500) {
        fprintf(stderr, "exiting because of server status %d (%s)\n",
                conn->status_code, http_truncate_response(conn));
        exit(1);
    }
}


// http_truncate_response(conn)
//    Truncate the `conn` response text to a manageable length and return
//    that truncated text. Useful for error messages.
char *http_truncate_response(http_connection *conn) {
    char *eol = strchr(conn->buf, '\n');
    if (eol)
        *eol = 0;
    if (strnlen(conn->buf, 100) >= 100)
        conn->buf[100] = 0;
    return conn->buf;
}


// main(argc, argv)
//    The main loop.
int main(int argc, char **argv) {
    // parse arguments
    int ch;
    while ((ch = getopt(argc, argv, "h:p:u:")) != -1) { 
        if (ch == 'h')
            pong_host = optarg;
        else if (ch == 'p')
            pong_port = optarg;
        else if (ch == 'u')
            pong_user = optarg;
        else
            usage();
    }
    if (optind == argc - 1)
        pong_user = argv[optind];
    else if (optind != argc)
        usage();

    // look up network address of pong server
    struct addrinfo ai_hints, *ai;
    memset(&ai_hints, 0, sizeof(ai_hints));
    ai_hints.ai_family = AF_INET;
    ai_hints.ai_socktype = SOCK_STREAM;
    ai_hints.ai_flags = AI_NUMERICSERV;
    int r = getaddrinfo(pong_host, pong_port, &ai_hints, &ai);
    if (r != 0) {
        fprintf(stderr, "problem contacting %s: %s\n",
                pong_host, gai_strerror(r));
        exit(1);
    }

    pthread_mutex_lock(&conn_tbl_lock);
    for(int i=0; i < MAX_CONNECTIONS;i++)
        conn_table[i] = NULL;
    pthread_mutex_unlock(&conn_tbl_lock);   

    // reset pong board and get its dimensions
    int width, height;
    {
        http_connection *conn = http_connect(ai);
        http_send_request(conn, "reset");
        http_receive_response(conn);
        if (conn->status_code != 200
            || sscanf(conn->buf, "%d %d\n", &width, &height) != 2
            || width <= 0 || height <= 0) {
            fprintf(stderr, "bad response to \"reset\" RPC: %d %s\n",
                    conn->status_code, http_truncate_response(conn));
            exit(1);
        }
        //http_close(conn);
    }

    // print display URL
    printf("Display: http://%s:%s/%s/\n", pong_host, pong_port, pong_user);

    // play game
    pthread_t worker;                   //worker threads
    pthread_attr_t attr;                //thread attributes
    pthread_mutex_lock(&lock);          //main thread has lock
    ball_thread *tdata = malloc(sizeof(ball_thread)); //allocate data for thread

    int x = 0, y = 0, dx = 1, dy = 1;
    //char url[BUFSIZ];
 
    int tcount = 0;

    tdata->ai= ai;  //pass a copy of the address info
    //tdata->url = malloc(BUFSIZ * sizeof(char)); //allocate memory for he URL buffer

    //set thread attributes
    setattr(&attr);
    while (1) {
        
        tdata->x = x;
        tdata->y = y; 
        tdata->seq = tcount;

       // Do a timedwait for phase 4
        pthread_mutex_lock(&pause_lock);
        int rc = pthread_cond_timedwait(&hold_flag,&pause_lock,&hold_time);
        if(rc == EINVAL || rc == EPERM || rc != ETIMEDOUT){
            perror("Error - Request did not wait.");
        }
        pthread_mutex_unlock(&pause_lock);

        //block waiting for the response flag
        //controls the number of threads
        pthread_create(&worker,&attr,move_ball,tdata);
        pthread_cond_wait(&flag,&lock);



        //update the next coordinate
        x += dx;
        y += dy;
        if (x < 0 || x >= width) {
            dx = -dx;
            x += 2 * dx;
        }
        if (y < 0 || y >= height) {
            dy = -dy;
            y += 2 * dy;
        }
        tcount++;
        // wait before moving to next frame
        // helps the any thread grab the hold
        // mutex
        sleep_for(0.2);
    
    }
    
    //close connections
    pthread_mutex_lock(&conn_tbl_lock);
    for(int i=0; i < MAX_CONNECTIONS;i++)
        http_close(conn_table[i]);
    pthread_mutex_unlock(&conn_tbl_lock);  

}

static void *move_ball(void *vpbt){

        //each thread has its own cordinates
        ball_thread *bt = (ball_thread*)vpbt; //recast the data for the thread
        int result = 0; // connection result
        while(!result){

        http_connection *conn = http_connect(bt->ai);

        sprintf(bt->url, "move?x=%d&y=%d&style=on", bt->x, bt->y);

        //printf("I am thread %d print at x=%d y=%d\n", bt->seq, bt->x, bt->y );
        http_send_request(conn, bt->url);

        http_receive_response(conn);

        if (conn->status_code != 200){
            if (conn->status_code == -1){
                result = 0;
                retry_wait(&bt->retry_count);
                pthread_mutex_lock(&conn_tbl_lock);
                for(int i=0;i<MAX_CONNECTIONS;i++){
                        if(conn_table[i] == conn){
                            conn_table[i] = NULL;
                            http_close(conn);
                        }
                }
                pthread_mutex_unlock(&conn_tbl_lock);
                //return NULL;
                continue;
            }
        } 
        else {
            char pause_string[20];
            char response_code[20];
            //printf("%s\n",conn->buf);
            if(sscanf(conn->buf,"%s %s",pause_string,response_code) == 2){
                if(strncmp("STOP",response_code,4) ==0){
                    pthread_mutex_lock(&pause_lock);
                    double hold = strtod(pause_string,NULL);
                    set_hold_time(hold);
                    pthread_mutex_unlock(&pause_lock);
                }
            }
        }


        result = 1;
        bt->retry_count = 0;

        double result = strtod(conn->buf, NULL);
        if (result < 0) {
            sleep_for(0.5);
            fprintf(stderr, "server returned error: %s\n",
                    http_truncate_response(conn));
            exit(1);
        }
    }
        return NULL;

}

//Function that calculates the stop time
//from the response
static void set_hold_time(double hold){
        //int hold_sec = (int)hold;
        //long hold_msec = (int)((hold - hold_sec) * 1000);
        //printf("Stop detected for %d sec %ld msec\n", hold_sec, hold_msec ); 
            struct timeval now;
            gettimeofday(&now,NULL);
            int uwait = hold * SECTOMICRO;
            int absmicro = uwait + now.tv_usec;
            int secpart = absmicro / SECTOMICRO;
            int mspart = absmicro % SECTOMICRO; 

            printf("Seconds is %d\n",secpart);
            hold_time.tv_sec = now.tv_sec + secpart;
            hold_time.tv_nsec = (mspart)* 1000;

}

//implements backoff for the retry
static void retry_wait(size_t *pretry_count){
    //static int num_retries = 0;
    //printf("Retry is %d\n",*pretry_count);
    //http_close(conn);
    double w_const = 0.001;
    double wait_time = ((1 << *pretry_count) * w_const);
    //printf("Wait time is %lf",wait_time);
    usleep(wait_time);
    (*pretry_count)++;

}

//set attributes for detached threads
static void setattr(pthread_attr_t *attrp){
    pthread_attr_init(attrp);
    pthread_attr_setdetachstate(attrp,PTHREAD_CREATE_DETACHED);
}

// TIMING AND INTERRUPT FUNCTIONS
static void handle_sigalrm(int signo);

// timestamp()
//    Return the current time as a real number of seconds.
double timestamp(void) {
    struct timeval now;
    gettimeofday(&now, NULL);
    return now.tv_sec + (double) now.tv_usec / 1000000;
}


// sleep_for(delay)
//    Sleep for `delay` seconds, or until an interrupt, whichever comes
//    first.
void sleep_for(double delay) {
    usleep((long) (delay * 1000000));
}


// interrupt_after(delay)
//    Cause an interrupt to occur after `delay` seconds. This interrupt will
//    make any blocked `read` system call terminate early, without returning
//    any data.
void interrupt_after(double delay) {static int signal_set = 0;
    if (!signal_set) {
        struct sigaction sa;
        sa.sa_handler = handle_sigalrm;
        sigemptyset(&sa.sa_mask);
        sa.sa_flags = 0;
        int r = sigaction(SIGALRM, &sa, NULL);
        if (r < 0) {
            perror("sigaction");
            exit(1);
        }
        signal_set = 1;
    }

    struct itimerval timer;
    timerclear(&timer.it_interval);
    timer.it_value.tv_sec = (long) delay;
    timer.it_value.tv_usec = (long) ((delay - timer.it_value.tv_sec) * 1000000);
    int r = setitimer(ITIMER_REAL, &timer, NULL);
    if (r < 0) {
        perror("setitimer");
        exit(1);
    }
}


// interrupt_cancel()
//    Cancel any outstanding interrupt.
void interrupt_cancel(void) {
    struct itimerval timer;
    timerclear(&timer.it_interval);
    timerclear(&timer.it_value);
    int r = setitimer(ITIMER_REAL, &timer, NULL);
    if (r < 0) {
        perror("setitimer");
        exit(1);
    }
}


// This is a helper function for `interrupt_after`.
static void handle_sigalrm(int signo) {
    (void) signo;
}


// HELPER FUNCTIONS FOR CODE ABOVE

// http_consume_headers(conn, eof)
//    Parse the response represented by `conn->buf`. Returns 1
//    if more data should be read into `conn->buf`, 0 if the response is
//    complete.
static int http_consume_headers(http_connection *conn, int eof) {
    size_t i = 0;
    while ((conn->state == HTTP_INITIAL || conn->state == HTTP_HEADERS)
           && i + 2 <= conn->len) {
        if (conn->buf[i] == '\r' && conn->buf[i+1] == '\n') {
            conn->buf[i] = 0;
            if (conn->state == HTTP_INITIAL) {
                int minor;
                if (sscanf(conn->buf, "HTTP/1.%d %d",
                           &minor, &conn->status_code) == 2){
                    conn->state = HTTP_HEADERS;
                    //send signal on getting a header
                    pthread_cond_signal(&flag);
                }
                else
                    conn->state = HTTP_BROKEN;
            } else if (i == 0)
                conn->state = HTTP_BODY;
            else if (strncmp(conn->buf, "Content-Length: ", 16) == 0) {
                conn->content_length = strtoul(conn->buf + 16, NULL, 0);
                conn->has_content_length = 1;
            }
            memmove(conn->buf, conn->buf + i + 2, conn->len - (i + 2));
            conn->len -= i + 2;
            i = 0;
        } else
            ++i;
    }

    if (conn->state == HTTP_BODY
        && (conn->has_content_length || eof)
        && conn->len >= conn->content_length)
        conn->state = HTTP_DONE;
    if (eof)
        conn->state = (conn->state == HTTP_DONE ? HTTP_CLOSED : HTTP_BROKEN);

    return conn->state >= 0;
}


// usage()
//    Explain how pong61 should be run.
static void usage(void) {
    fprintf(stderr, "Usage: ./pong61 [-h HOST] [-p PORT] [USER]\n");
    exit(1);
}
